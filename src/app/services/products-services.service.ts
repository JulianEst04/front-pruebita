import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { ProductoModel } from '../models/producto.model';

@Injectable({
  providedIn: 'root'
})

export class ProductsServicesService {
  private url = 'http://localhost:3000';

  constructor(private http:HttpClient) {}

    crearProducto( product:any ){
     return this.http.post(`${this.url}/productos`,product);
    }

    actualizarProducto(product:ProductoModel){
    return this.http.put(`${this.url}/productos/${product.id}`,product);
    }

    getProducts(){
      return this.http.get(`${this.url}/productos`);
    }
    

    getProduct(id:string){ 
      return this.http.get(`${this.url}/productos/${id}`);
    }

    borrarProducto(id:string){
      return this.http.delete(`${this.url}/productos/${id}`);
    }
    
   
   
}
