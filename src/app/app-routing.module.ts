import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioAppComponent } from './pages/inicio-app/inicio-app.component';


const routes: Routes = [
  { path:'app',
     loadChildren: () => import('./pages/inicio-app/app-core.module').then(m => m.AppCoreModule)
  },
  {
    path:'**',pathMatch:'full', redirectTo:'app'
  },
  {
    path:'',pathMatch:'full', redirectTo:'app'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
