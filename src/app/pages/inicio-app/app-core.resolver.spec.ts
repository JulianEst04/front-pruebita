import { TestBed } from '@angular/core/testing';

import { AppCoreResolver } from './app-core.resolver';

describe('AppCoreResolver', () => {
  let resolver: AppCoreResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(AppCoreResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
