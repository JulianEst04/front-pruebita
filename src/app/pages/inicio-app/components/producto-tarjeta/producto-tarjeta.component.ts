import { Component, Input, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-producto-tarjeta',
  templateUrl: './producto-tarjeta.component.html',
  styleUrls: ['./producto-tarjeta.component.scss']
})


export class ProductoTarjetaComponent implements OnInit {

  @Input() producto:any = {}
  @Input() index:number= 0;
  @Output() selectedProducto: EventEmitter<object> ;

  constructor() { 
    this.selectedProducto = new EventEmitter();
  }
  ngOnInit(): void {
  }
  //Borrar Producto
  borrar(){
    this.selectedProducto.emit( {producto: this.producto, index: this.index} );
  }
}
