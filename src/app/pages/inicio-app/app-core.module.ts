import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingRoutingModule } from './app-routing-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//components
import { ProductoTarjetaComponent } from './components/producto-tarjeta/producto-tarjeta.component';
import { NavbarComponent } from './components/navbar/navbar.component';

//Pages
import { ProductoComponent } from './producto/producto.component';
import { ProductosComponent } from './productos/productos.component';
import { InicioAppComponent } from './inicio-app.component';

//Angular material.
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';



@NgModule({
  declarations: [
    ProductoComponent,
    ProductosComponent,
    InicioAppComponent,
    NavbarComponent,
    ProductoTarjetaComponent
  ],
  imports: [
    CommonModule,
    MatMenuModule,
    MatToolbarModule,
    RouterModule,
    FormsModule,
    AppRoutingRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class AppCoreModule { }
