import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioAppComponent } from './inicio-app.component';
import { ProductoComponent } from './producto/producto.component';
import { ProductosComponent } from './productos/productos.component';

const routes: Routes = [
  {
    path: '',
    component: InicioAppComponent,
    children : [
      {
        path: 'producto/:id',
        component: ProductoComponent 
      },
      {
        path: 'productos',
        component: ProductosComponent 
      },
      {
        path: '**', pathMatch: 'full', redirectTo: ''
      }  
    ] 
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingRoutingModule { }
