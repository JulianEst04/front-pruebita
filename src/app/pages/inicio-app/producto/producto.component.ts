import { Component, OnChanges, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductoModel } from 'src/app/models/producto.model';
import { ProductsServicesService } from 'src/app/services/products-services.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.scss']
})
export class ProductoComponent implements OnInit {
  
  producto:ProductoModel = new ProductoModel();
  id:any = 0;
  productoObtenido:any = {};
  selected:string = '';
  producto1: ProductoModel[] = [];
  forma:FormGroup = this.fb.group({});

  constructor( 
    private productService:ProductsServicesService,
    private route:ActivatedRoute,
    private fb: FormBuilder,
    private router:Router
    ) { }

  ngOnInit(): void {
    this.postProduct();
    this.productService.getProducts()
      .subscribe((resp:any) =>{
        this.producto1 = resp;
     })
  }


  get productoNoValido() {
    return this.forma.get('name')?.invalid && this.forma.get('name')?.touched
  };
  get precioNoValido() {
    return this.forma.get('price')?.invalid && this.forma.get('price')?.touched
  };

  postProduct(){
    this.forma = this.fb.group({
      id:[this.productoObtenido.id],
      name:['', [Validators.required]],
      price:['', [Validators.required]],
      img:['']
    }
    )
  }

//Carga producto unico con id unico
    cargar(valor:any){
      this.productService.getProduct(valor)
      .subscribe((resp =>{
        this.productoObtenido = resp;
        if(this.selected){
          this.router.navigateByUrl(`app/producto/${this.productoObtenido.id}`);
        }else{
          this.router.navigateByUrl(`app/producto/nuevo`);
        }
      }))
    }
//Guardar producto o actualizar
    guardar(){
      if(this.forma.invalid){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Por favor llena todos los campos!!',
        })
      return;
    }
    Swal.fire({
      title: 'Espere',
      text: 'Guardando...',
      icon: 'info',
      allowOutsideClick:false
    })
    Swal.showLoading();

    if( this.productoObtenido.id ){
      this.forma.get('id')?.setValue(this.productoObtenido.id);
      this.productService.actualizarProducto(this.forma.value)
      .subscribe((res:any) =>{
        this.productoObtenido = res;
        Swal.fire({
      title: this.productoObtenido.name,
      text: 'Se Actualizo correctamente',
      icon: 'success',
    })
      })
    }else{
      this.productService.crearProducto(this.forma.value)
      .subscribe((res:any) =>{
        Swal.fire({
          title: this.producto.name,
          text: 'Se guardo correctamente',
          icon: 'success',
        })
      })
    }
  }
  
}
