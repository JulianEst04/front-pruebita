import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoModel } from 'src/app/models/producto.model';
import { ProductsServicesService } from 'src/app/services/products-services.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  
  id:any = 0;
  products: ProductoModel[] = [];
  constructor( private productsServices:ProductsServicesService, private router: Router) { }

  ngOnInit(): void {
    this.productsServices.getProducts()
    .subscribe((resp:any) =>{
      this.products = resp;
    })
  }
  borrar(producto:any){
    Swal.fire({
      title: 'Estas seguro?',
      text: "Deseas borrar este delicioso producto?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar.!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.products.splice(producto.index,1);
        this.productsServices.borrarProducto(producto.producto.id)
        .subscribe();
      }
    })
 
  }
}
